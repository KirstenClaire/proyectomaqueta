//COLEGIO LA SALLE DE MARGARITA
//           FISICA
//      CODIGO DE MAQUETA
//    PARQUE DE DIVERSIONES

#include <IRremote.h> //Incluimos libreria para el control infrarojo
int RECV_PIN = 0; //asignamos el pin que recibira la señal del modulo infrarojo
IRrecv irrecv(RECV_PIN); //declaramos el pin para que pueda recibir los resultados del infrarojo
decode_results results; //declaramos la variable results para poder accionar la decodificacion de resultados
uint32_t Previous;//declaramos una variable llamada previous, para evitar la mala interpretacion del infrarojo

//Procedemos a definir uno por uno los codigos hexadecimales por cada boton que usaremos de nuestro infrarojo
#define Button1 0xFF30CF //GirarKamikaziDer
#define Button2 0xFF18E7 //PararKamikazi
#define Button3 0xFF7A85 //GirarKamikaziIzq
#define Button4 0xFF10EF //GirarVertigoDer
#define Button5 0xFF38C7 //PararVertigo
#define Button6 0xFF5AA5 //GirarVertigoIzq
#define Button7 0xFF42BD //GirarRuedaFortunaDer
#define Button8 0xFF4AB5 //PararRuedaFortuna
#define Button9 0xFF52AD //GirarRuedaFortunaIzq
#define ButtonPLUS  0xFFA857 //EncenderVolcan
#define ButtonMINUS 0xFFE01F //PararVolcan
// Motor -  Kamikazi
int ENA = 2;
int IN1 = 3;
int IN2 = 4;

// Motor - Vertigo
int ENB = 5;
int IN3 = 6;
int IN4 = 7;

//Motor - Rueda de la Fortuna
int ENC = 8;
int IN5 = 9;
int IN6 = 10;

//Bomba de Agua - Volcan
int END = 1;
int IN7 = 12;
int IN8 = 13;


//Procedemos a establecer el funcionamiento que le daremos a los pines de arduino
void setup ()
{
  Serial.begin(9600);  //starts serial communication
  
 // Declaramos todos los pines como salidas
 pinMode (ENA, OUTPUT);
 pinMode (ENB, OUTPUT);
 pinMode (ENC, OUTPUT);
 pinMode (END, OUTPUT);
 pinMode (IN1, OUTPUT);
 pinMode (IN2, OUTPUT);
 pinMode (IN3, OUTPUT);
 pinMode (IN4, OUTPUT);
 pinMode (IN5, OUTPUT);
 pinMode (IN6, OUTPUT);
 pinMode (IN7, OUTPUT);
 pinMode (IN8, OUTPUT);

irrecv.enableIRIn(); // Comienza a recibir los datos
}

//Declaramos los voids de funcionamiento para cada atraccion de nuestra maqueta:
void GirarKamikaziDer()
{
 digitalWrite (IN1, HIGH);
 digitalWrite (IN2, LOW);
 analogWrite (ENA, 130); //Velocidad
}
void GirarKamikaziIzq()
{
 digitalWrite (IN1, LOW);
 digitalWrite (IN2, HIGH);
 analogWrite (ENA, 205); //Velocidad
}

void GirarVertigoDer()
{
 digitalWrite (IN3, HIGH);
 digitalWrite (IN4, LOW);
 analogWrite (ENB, 200); //Velocidad
}
void GirarVertigoIzq()
{
 digitalWrite (IN3, LOW);
 digitalWrite (IN4, HIGH);
 analogWrite (ENB, 200); //Velocidad
}

void GirarRuedaFortunaDer()
{
 digitalWrite (IN5, HIGH);
 digitalWrite (IN6, LOW);
 analogWrite (ENC, 200); //Velocidad
}

void GirarRuedaFortunaIzq()
{
 digitalWrite (IN5, LOW);
 digitalWrite (IN6, HIGH);
 analogWrite (ENC, 200); //Velocidad
}

void EncenderVolcan()
{
 digitalWrite (IN7, HIGH);
 digitalWrite (IN8, LOW);
 analogWrite (END, 200); //Velocidad
}


void PararKamikazi ()
{
 digitalWrite (IN1, LOW);
 digitalWrite (IN2, LOW);
 analogWrite (ENA, 0); //Velocidad
}

void PararVertigo ()
{
 digitalWrite (IN3, LOW);
 digitalWrite (IN4, LOW);
 analogWrite (ENA, 0); //Velocidad
}

void PararRuedaFortuna ()
{
 digitalWrite (IN5, LOW);
 digitalWrite (IN6, LOW);
 analogWrite (ENA, 0); //Velocidad
}

void PararVolcan ()
{
 digitalWrite (IN7, LOW);
 digitalWrite (IN8, LOW);
 analogWrite (ENA, 0); //Velocidad
}

//Loop Principal de Funcionamiento
void loop() {

if (irrecv.decode(&results)) { //Decodifica las señales enviadas por el sensor infrarojo
 if(results.value == 0xFFFFFFFF){ //Condicion: Si, el resultado del modulo infrarojo es igual a 0xFFFFFFFFF, entonces:
  results.value=Previous;                                                                                    //el resultado va a ser igual a la lectura previa a este valor 
  }
  
//Estructura de control para determinar los movimientos a realizar segun el numero de boton presionado
switch(results.value){
  case Button1:
  GirarKamikaziDer();
  break;

  case Button2:
  PararKamikazi();
  break;

  case Button3:
  GirarKamikaziIzq();
  break;

  case Button4:
  GirarVertigoDer();
  break;

  case Button5:
  PararVertigo();
  break;

  case Button6:
  GirarVertigoIzq();
  break;

  case Button7:
  GirarRuedaFortunaDer();
  break;

  case Button8:
  PararRuedaFortuna();
  break;
  
  case Button9:
  GirarRuedaFortunaIzq();
  break;

  case ButtonPLUS:
  EncenderVolcan();
  break;

  case ButtonMINUS:
  PararVolcan();
  break;
}
Serial.println(results.value, HEX); // imprime el valor en codigo hexadecimal dado por el control infrarojo. Este se puede visualizar en el monitor de arduino
irrecv.resume(); // Preparara al arduino para recibir el siguiente valor atraves del sensor
}
}
